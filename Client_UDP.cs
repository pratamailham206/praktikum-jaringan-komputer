using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpClient
{
    public static void Main()
    {
        byte[] data = new byte[1024];
        byte[] username = new byte[1024];
        string ch, user;
        string input, stringData;
        IPEndPoint ipep = new IPEndPoint(
                        IPAddress.Parse("192.168.43.84"), 8080);

        Socket server = new Socket(AddressFamily.InterNetwork,
                       SocketType.Dgram, ProtocolType.Udp);




        //mengirim
        string welcome = "Hello";
        data = Encoding.ASCII.GetBytes(welcome);
        server.SendTo(data, data.Length, SocketFlags.None, ipep);

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)sender;

        data = new byte[1024];
        int recv = server.ReceiveFrom(data, ref Remote);

        Console.WriteLine("Message received from {0}:", Remote.ToString());
        Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));

        //kirim username
        Console.Write("input username : ");
        user = Console.ReadLine();
        server.SendTo(Encoding.ASCII.GetBytes(user), Remote);

        //terima
        username = new byte[1024];
        int terima = server.ReceiveFrom(username, ref Remote);
        ch = Encoding.ASCII.GetString(username, 0, terima);
        Console.WriteLine("username server : " + Encoding.ASCII.GetString(username, 0, terima));

        Console.WriteLine("mulai Chat!");
        while (true)
        {
            //mengirim
            Console.Write(user + " : ");
            input = Console.ReadLine();
            if (input == "exit")
                break;
            server.SendTo(Encoding.ASCII.GetBytes(input), Remote);

            //menerima
            data = new byte[1024];
            recv = server.ReceiveFrom(data, ref Remote);
            stringData = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine(ch + " : " + stringData);
        }
        Console.WriteLine("Stopping client");
        server.Close();
    }
}